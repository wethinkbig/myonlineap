export const environment = {
  production: true,
  apiUrl: 'http://localhost:3000/api/onlineap',
  socketUrl : 'https://localhost:3000'
  // apiUrl: 'http://localhost:4000'
};
