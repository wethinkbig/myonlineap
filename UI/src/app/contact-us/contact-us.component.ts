import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {ContactService} from './contact.service';
import * as AOS from 'aos';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.less']
})
export class ContactUsComponent implements OnInit {

  form : FormGroup;
  successMessage= null;
  loading= false;
  waiting = false;
  constructor(private contact : ContactService ) { }

  ngOnInit(): void {
  AOS.init({
    duration : 1000
  })

    this.form = new FormGroup({
      'fullname' : new FormControl(null , Validators.required),
      'email' : new FormControl(null , [Validators.required , Validators.email]),
      'query' : new FormControl(null , Validators.required)
    })
  }

  get f(){
    return this.form.controls;
  }

  onSubmit(){
    if(this.form.valid){
      this.loading = true;
      this.waiting = true;
      this.contact.onSend(this.form.value).subscribe( data => {
        this.successMessage = "Thank your for contacting us ! Your query with email : "+data.result+" was registered successfully 🙂.Admin will reach you out soon";
        setTimeout(() => {
          this.successMessage = null;
        }, 6000);
        this.form.reset();
        this.waiting = false;
        this.loading = false;
      } , error => {
        console.log(error);
        
      })
      
    }

  }

}
