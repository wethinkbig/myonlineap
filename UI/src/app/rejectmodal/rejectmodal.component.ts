import { Component, OnInit , Inject } from '@angular/core';
import {MatDialogRef , MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-rejectmodal',
  templateUrl: './rejectmodal.component.html',
  styleUrls: ['./rejectmodal.component.less']
})
export class RejectmodalComponent implements OnInit {

  reasons  = '';

  constructor(
    public dialogRef : MatDialogRef<RejectmodalComponent>,
    @Inject(MAT_DIALOG_DATA) public data:any
  ) { }

  ngOnInit(): void {
  }

  onReject(){
    this.dialogRef.close({
      _id : this.data._id,
      reasons : this.reasons
    })
  }

}
