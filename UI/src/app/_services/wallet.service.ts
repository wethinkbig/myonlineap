import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@environments/environment';


@Injectable({
  providedIn : 'root'
})
export class WalletService {

  constructor(private http : HttpClient){}

  loadWallet(body) : Observable<any>{
    return this.http.post(`${environment.apiUrl}/loadwallet` , body);
  }
  getAllWallets() : Observable<any>{
    return this.http.get(`${environment.apiUrl}/getwallets`);
  }
  updateWallet(body) : Observable<any>{
    return this.http.put(`${environment.apiUrl}/updatewallet` , body);
  }



}
