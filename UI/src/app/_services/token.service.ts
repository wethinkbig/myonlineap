import { Injectable } from '@angular/core';
import {CookieService} from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  constructor(private cookieservice : CookieService) { }
  setToken(token){
    this.cookieservice.set('onlineap_token' , token);
  }
  getToken(){
    return this.cookieservice.get('onlineap_token');
  }
  deleteToken(){
    return this.cookieservice.delete('onlineap_token');
  }
  getPayload(){
    const token = this.getToken();
    let payload;
    if(token){
      payload = token.split('.')[1];
      payload = JSON.parse(window.atob(payload));
    }
    return payload.data;
  }
}
