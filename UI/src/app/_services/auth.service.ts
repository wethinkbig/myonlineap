import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class AuthService{
    constructor(private http : HttpClient){}

    registerUser(body) :Observable<any>{
        return this.http.post(`${environment.apiUrl}/register`, body);
    }
    loginUser(body):Observable<any>{
        return this.http.post(`${environment.apiUrl}/login`, body);
    }
    forgotpassword(body) : Observable<any>{
        return this.http.put(`${environment.apiUrl}/forgot-password` , body);
    }
    resetpassword(body) : Observable<any>{
        return this.http.put(`${environment.apiUrl}/reset-password` , body);
    }
}