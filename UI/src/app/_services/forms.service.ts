import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable, OnInit } from "@angular/core";
import { environment } from "@environments/environment";
import { Observable } from "rxjs";


@Injectable({
  providedIn : 'root'
})
export class FormsService implements OnInit{

  constructor(private http : HttpClient){}

  ngOnInit(){
  }

  generatePdf(body):Observable<any>{
    return this.http.post(`${environment.renderingEngineAPi}/render` , body  , {
      responseType : 'blob',
      headers : new HttpHeaders().append('Content-Type' , 'application/json')
    } );
  }

  submitForm(body) : Observable<any>{
    return this.http.post(`${environment.apiUrl}/submitform` , body);
  }
  
  uploadForm(body) : Observable<any>{
    return this.http.post(`${environment.apiUrl}/uploadform` , body);
  }
  getUploadedForms():Observable<any>{
    return this.http.get(`${environment.apiUrl}/getUploadedForms`);
  }

  updateFormStatus(body) : Observable<any>{
    return this.http.put(`${environment.apiUrl}/updateFormStatus` , body);
  }




}
