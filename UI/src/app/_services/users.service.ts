import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http : HttpClient) { }

  GetAllUsers() : Observable<any>{
    return this.http.get(`${environment.apiUrl}/users`);
  }
  GetUserById(id) : Observable<any>{
    return this.http.get(`${environment.apiUrl}/user/${id}`);
  }
  GetUserByName(username) : Observable<any>{
    return this.http.get(`${environment.apiUrl}/username/${username}`);
  }

  updateUser(id , body) : Observable<any>{
    return this.http.put(`${environment.apiUrl}/update/${id}`, body);
  }

  deleteUser(id) :Observable<any>{
    return this.http.delete(`${environment.apiUrl}/delete/${id}`);
  }

}
