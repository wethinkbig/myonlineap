import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LayoutComponent } from './layout.component';
import{WalletrequestComponent} from './walletrequest/walletrequest.component';
import {FormrequestsComponent } from "./formrequests/formrequests.component";

const accountsModule = () => import('./accounts/accounts.module').then(x => x.AccountsModule);

const routes: Routes = [
    {
        path: '', component: LayoutComponent,
        children: [
            { path: 'accounts', loadChildren: accountsModule },
            {path : 'walletrequests' , component : WalletrequestComponent},
            {path : 'formrequests' , component : FormrequestsComponent}
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdminRoutingModule { }
