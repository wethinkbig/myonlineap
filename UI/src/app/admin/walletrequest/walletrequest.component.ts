import { Component, OnInit, ViewChild , AfterViewInit} from '@angular/core';
import {WalletService} from '../../_services/wallet.service';
import { Router} from '@angular/router';
import {UploadService} from '../../_services/upload.service';
import { saveAs } from 'file-saver';
import{Buffer} from 'buffer';
import{MatTableDataSource} from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialog } from '@angular/material/dialog';
import { RejectmodalComponent } from '@app/rejectmodal/rejectmodal.component';
import {io} from 'socket.io-client';
import { environment } from '@environments/environment';

@Component({
  selector: 'app-walletrequest',
  templateUrl: './walletrequest.component.html',
  styleUrls: ['./walletrequest.component.less']
})

export class WalletrequestComponent implements OnInit , AfterViewInit {

  displayedColumns: string[] = ['amount','referenceid','email','actions','status'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  wallets : any = [];
  dataSource: MatTableDataSource<any>;
  reasons = '';
  walletId = '';
  allWallets : any;
  socket : any;

  constructor(private wallet : WalletService,public dialog:MatDialog,
            private router : Router,
            private uploadservice : UploadService,
            ) {
                this.socket = io(`${environment.socketUrl}`);
             }

            openDialog(sendData): void {
              const dialogRef = this.dialog.open(RejectmodalComponent, {
                width: '560px',
                data: { _id : sendData }
              });
          
              dialogRef.afterClosed().subscribe(result => {
                if(result._id && result.reasons){
                    this.walletId = result._id;
                    this.reasons = result.reasons;
                    this.onReject();
                }
                
              });
            }

  

  ngOnInit(): void {
    this.wallet.getAllWallets().subscribe(data => {
      this.dataSource = new MatTableDataSource(data.result);
      this.wallets = data.result;
    } , err => {
      // console.log(err);
    });
    this.socket.on('refreshPage' , () => {
      this.wallet.getAllWallets().subscribe(data => {
        this.dataSource = new MatTableDataSource(data.result);
        this.dataSource.paginator = this.paginator;
        this.wallets = data.result;
      } , err => {
        console.log(err);
      });
    })
  }
  ngAfterViewInit(){
    setTimeout(() => {
      this.dataSource.paginator = this.paginator;
    }, 300);
  }

  applyFilter(filterValue : string){
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  onApprove(uniqueid : string , amount : number , userid : string ){

    const body = {
        id : uniqueid,
        status : 'Approved',
        reasons : '',
        userid : userid,
        amount : amount
    }
    this.wallet.updateWallet(body).subscribe(data => {
      this.socket.emit('refresh' , {});
    } , err=> {
      console.log(err);
    })
  }

  onReject(){

    const body = {
      id :  this.walletId,
      status : 'Rejected',
      reasons : this.reasons
    }
    this.wallet.updateWallet(body).subscribe(data => {
      this.socket.emit('refresh' , {});
    } , err => {
      console.log(err);
    })

  }
  async getImage(imageName : string){
    const img = await  this.uploadservice.getFile(imageName , "Wallets");
    console.log(img.Body);
    var blob = new Blob([ this.encode(img.Body) ]);
    saveAs(blob,imageName);
  }
  encode(data){
    let buf = Buffer.from(data);
    let base64 = buf.toString('base64');
    return buf;
    }
}
