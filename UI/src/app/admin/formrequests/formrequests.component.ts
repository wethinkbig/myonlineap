import { Component, OnInit , AfterViewInit} from '@angular/core';
import {FormsService} from '../../_services/forms.service';
import { Router} from '@angular/router';
import {UploadService} from '../../_services/upload.service';
import { saveAs } from 'file-saver';
import{Buffer} from 'buffer';
import{MatTableDataSource} from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { ViewChild } from '@angular/core';
import {io} from 'socket.io-client';
import { environment } from '@environments/environment';
import { MatDialog } from '@angular/material/dialog';
import { RejectmodalComponent } from '@app/rejectmodal/rejectmodal.component';

@Component({
  selector: 'app-formrequests',
  templateUrl: './formrequests.component.html',
  styleUrls: ['./formrequests.component.less']
})
export class FormrequestsComponent implements OnInit , AfterViewInit {

  forms : any  = [];
  socket : any;
  reasons  = '';
  formRejectionId = '';
  selectedFiles : any;
  clickUpload = false;

  displayedColumns: string[] = ['fullname', 'email','actions','status'];

  dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(private formsservice : FormsService,
            private router : Router, public dialog:MatDialog,
            private uploadservice : UploadService) {
                this.socket = io(`${environment.socketUrl}`);
              
}

openDialog(sendData): void {
  const dialogRef = this.dialog.open(RejectmodalComponent, {
    width: '560px',
    data: { _id : sendData }
  });

  dialogRef.afterClosed().subscribe(result => {
    if(result._id && result.reasons){
      this.formRejectionId = result._id;
        this.reasons = result.reasons;
        this.onReject();
    }
    
  });
}
  ngOnInit(): void {
        this.formsservice.getUploadedForms().subscribe(data => {
          console.log(data.uploadedforms);
          
        this.dataSource = new MatTableDataSource(data.uploadedforms.reverse());
        this.forms = data.uploadedforms.reverse();
        } , err=> {
          console.log(err);
        })
        this.socket.on('refreshPage' , () => {
                this.formsservice.getUploadedForms().subscribe(data => {
                this.dataSource = new MatTableDataSource(data.uploadedforms);
                this.dataSource.paginator = this.paginator;
            })
        })
  }

  ngAfterViewInit(){
    setTimeout(() => {
      this.dataSource.paginator = this.paginator;
    }, 500);

  }
  onChange(event){
    this.selectedFiles = event.target.files;
    const file = this.selectedFiles.item(0);
    var filename= file.name;
  }
  onApprove(uniqueid : string){
    if(!this.selectedFiles){
      alert("Please select the FILE");
    }else{
    const file = this.selectedFiles.item(0);
          this.uploadservice.fileUpload(file , 'Acknowledgements');
      const fileName = file.name;
      var filename = fileName;
    const body = {
      id : uniqueid,
      status : 'Approved',
      filename : filename
    }
    this.formsservice.updateFormStatus(body).subscribe(data => {
      this.selectedFiles = '';
      this.clickUpload = false;
      this.socket.emit('refresh' , {})
    } , err => {
      console.log(err);
    });

    }

  
  }

 

  
  onReject(){
    const body = {
      id : this.formRejectionId,
      status : 'Rejected',
      reasons : this.reasons
    }
    this.formsservice.updateFormStatus(body).subscribe(data => {
      this.socket.emit('refresh' , {})
    } , err => {
      console.log(err);
    })
  }
  async getFile(fileName : string , jpgfileName : string){
    const file = await  this.uploadservice.getFile(fileName , 'Forms/pdf');
    console.log(file.Body );
    var blob = new Blob([ this.encode(file.Body) ]);
    saveAs(blob,fileName);
    const jpgfile = await  this.uploadservice.getFile(jpgfileName , 'Forms/jpg');
    console.log(file.Body );
    var blob = new Blob([ this.encode(jpgfile.Body) ]);
    saveAs(blob,jpgfileName);


  }
  encode(data){
    let buf = Buffer.from(data);
    let base64 = buf.toString('base64');
    return buf;
    }

}
