import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { FormsService } from '@app/_services/forms.service';
import { UploadService } from '@app/_services/upload.service';
import * as AOS from 'aos';
import {io} from 'socket.io-client';
import {saveAs} from 'file-saver';
import { environment } from '@environments/environment';

@Component({
  selector: 'app-submitform',
  templateUrl: './submitform.component.html',
  styleUrls: ['./submitform.component.less']
})
export class SubmitformComponent implements OnInit {
  Pdffilename = "No Pdf Selected";
  Jpgfilename = "No Image Selected";
  selectedPdfFiles : any;
  selectedJpgFiles : any;
  formtypes = ["newform" , "correctionform"];

  selectedFormType = '';
  showform = false;

  loading = false;
  genders = ["Male","Female","Transgender"];
  status = ["Individual" , "Hindu undivided family" , "Company" , "Partnership Firm" , "Government" , "Trusts" , "Body of Individuals" , 
            "Local Authority" , "Artificial Juridical Persons" , "Limited Liability Partnership"]

  sources = ["1.Salary" , "2.Capital Gains" , "3.Income from Business / Profession" , "4.Income from House property" , "5.Income from Other sources" , "6.No income"]

  success:string = null;

  generatePdfForm : FormGroup;
  socket : any;
  alias : any = '';
  aadharalloted = '';
  motherSingleParent : any = '';
  officeAddress : any = '';

  constructor(private _formBuilder : FormBuilder, private formsservice : FormsService,
              private snap : ActivatedRoute, private fb : FormBuilder,
              private uploadservice :  UploadService,
              private router : Router) { 
                this.socket = io(environment.socketUrl);
              }

  ngOnInit(): void {
    AOS.init({
      duration : 1000
    })
  }

  onChangeFormType(){
    this.showform = true;
    if(this.selectedFormType == 'correctionform'){
      this.generateFormInit();
      this.generatePdfForm.addControl('photo_mismatch' , new FormControl('' , Validators.required ));
      this.generatePdfForm.addControl('signature_mismatch' , new FormControl('' , Validators.required ));
      this.generatePdfForm.addControl('office_name' , new FormControl('' ));
      this.generatePdfForm.addControl('aadhar_alloted' , new FormControl('' , Validators.required ));
      this.generatePdfForm.addControl('pan1' , new FormControl('' , Validators.required ));
      this.generatePdfForm.addControl('pan2' , new FormControl('' , Validators.required ));
      this.generatePdfForm.addControl('pan3' , new FormControl('' , Validators.required ));

      this.generatePdfForm.removeControl('alias_');
      this.generatePdfForm.removeControl('office_address');
      this.generatePdfForm.removeControl(' status_of_applicant');
      this.generatePdfForm.removeControl('source_of_income');
      this.generatePdfForm.removeControl('registration_number ');
      this.generatePdfForm.removeControl('enrollment_number');
      this.generatePdfForm.removeControl('ra_title');
      this.generatePdfForm.removeControl('ra_surname');
      this.generatePdfForm.removeControl('ra_first_name');
      this.generatePdfForm.removeControl('ra_middle_name');
      this.generatePdfForm.removeControl('ra_address_line_one');
      this.generatePdfForm.removeControl('ra_address_line_two');
      this.generatePdfForm.removeControl('ra_address_line_three');
      this.generatePdfForm.removeControl('ra_address_line_four');
      this.generatePdfForm.removeControl('ra_address_line_five');
      this.generatePdfForm.removeControl('ra_state');
      this.generatePdfForm.removeControl('ra_pincode');
    }else{
      this.generateFormInit();
    }
    
  }

  generateFormInit(){

    this.generatePdfForm =this.fb.group({
      original_title : [null , Validators.required],
      original_surname : [null , Validators.required],
      original_first_name : [null , Validators.required],
      original_middle_name : [null , Validators.required],
      pan_card_abbrevation : [null , Validators.required],
      alias_ : [null , Validators.required],
      gender : [null , Validators.required],
      DOB : [null , Validators.required],
      motherSingleParent_ : [null , Validators.required],
      selection_name : [null , Validators.required],
      address_line_one : [null , Validators.required],
      address_line_two : [null , Validators.required],
      address_line_three : [null , Validators.required],
      address_line_four : [null , Validators.required],
      address_line_five : [null , Validators.required],
      state : [null , Validators.required],
      pincode : [null , Validators.required],
      country_name : [null , Validators.required],
      office_address : [null , Validators.required],
      address_of_communication : [null , Validators.required],
      country_code : [null , Validators.required],
      std_code : [null , Validators.required],
      mobile_number : [null , Validators.required],
      email_id : [null , [Validators.required , Validators.email]],
      status_of_applicant : [null ,Validators.required],
      registration_number : [null ,Validators.required],
      aadhar_number : [null ,Validators.required],
      enrollment_number : [null ,Validators.required],
      name_on_aadhar : [null ,Validators.required],
      ra_title : [null , Validators.required],
      source_of_income : [null , Validators.required],
      ra_surname : [null , Validators.required],
      ra_first_name : [null , Validators.required],
      ra_middle_name : [null , Validators.required],
      ra_address_line_one : [null , Validators.required],
      ra_address_line_two : [null , Validators.required],
      ra_address_line_three : [null , Validators.required],
      ra_address_line_four : [null , Validators.required],
      ra_address_line_five : [null , Validators.required],
      ra_state : [null , Validators.required],
      ra_pincode : [null , Validators.required],

    })



  }

  checkaadharalloted(){
    this.aadharalloted = this.generatePdfForm.get('aadhar_alloted').value;
  }
  aliasCheck(){
    this.alias = this.generatePdfForm.get('alias_').value;
    if(this.alias == "true"){
      this.generatePdfForm.addControl('alias_title' , new FormControl('' , Validators.required));
      this.generatePdfForm.addControl('alias_surname' , new FormControl('' , Validators.required));
      this.generatePdfForm.addControl('alias_first_name' , new FormControl('' , Validators.required));
      this.generatePdfForm.addControl('alias_middle_name' , new FormControl('' , Validators.required));
    }else{
      this.generatePdfForm.removeControl('alias_title');
      this.generatePdfForm.removeControl('alias_surname');
      this.generatePdfForm.removeControl('alias_first_name');
      this.generatePdfForm.removeControl('alias_middle_name');

    }
  }

  checkMotherSingleParent(){
    this.motherSingleParent = this.generatePdfForm.get('motherSingleParent_').value;
    if(this.motherSingleParent == "true"){
      this.generatePdfForm.removeControl('DOP_father_surname');
      this.generatePdfForm.removeControl('DOP_father_first_name');
      this.generatePdfForm.removeControl('DOP_father_middle_name');
      this.generatePdfForm.addControl('​DOP_mother_surname' , new FormControl('' , Validators.required));
      this.generatePdfForm.addControl('DOP_mother_first_name' , new FormControl('' , Validators.required));
      this.generatePdfForm.addControl('DOP_mother_middle_name' , new FormControl('' , Validators.required));


    }else{
      this.generatePdfForm.addControl('DOP_father_surname' , new FormControl('' , Validators.required));
      this.generatePdfForm.addControl('DOP_father_first_name' , new FormControl('' , Validators.required));
      this.generatePdfForm.addControl('DOP_father_middle_name' , new FormControl('' , Validators.required));
      this.generatePdfForm.addControl('​DOP_mother_surname' , new FormControl('' , Validators.required));
      this.generatePdfForm.addControl('DOP_mother_first_name' , new FormControl('' , Validators.required));
      this.generatePdfForm.addControl('DOP_mother_middle_name' , new FormControl('' , Validators.required));

    }
    
    
  }

  checkOfficeAddress(){
    this.officeAddress = this.generatePdfForm.get('office_address').value;
    if(this.officeAddress == "true"){
      this.generatePdfForm.addControl('office_address_line_one' , new FormControl('' , Validators.required));
      this.generatePdfForm.addControl('office_address_line_two' , new FormControl('' , Validators.required));
      this.generatePdfForm.addControl('office_address_line_three' , new FormControl('' , Validators.required));
      this.generatePdfForm.addControl('office_address_line_four' , new FormControl('' , Validators.required));
      this.generatePdfForm.addControl('office_address_line_five' , new FormControl('' , Validators.required));
      this.generatePdfForm.addControl('office_address_line_six' , new FormControl('' , Validators.required));
      this.generatePdfForm.addControl('office_state' , new FormControl('' , Validators.required));
      this.generatePdfForm.addControl('office_pincode' , new FormControl('' , Validators.required));
      this.generatePdfForm.addControl('office_country_name' , new FormControl('' , Validators.required));

    }else{
      this.generatePdfForm.removeControl('office_address_line_one');
      this.generatePdfForm.removeControl('office_address_line_two');
      this.generatePdfForm.removeControl('office_address_line_three');
      this.generatePdfForm.removeControl('office_address_line_four');
      this.generatePdfForm.removeControl('office_address_line_five');
      this.generatePdfForm.removeControl('office_address_line_six');
      this.generatePdfForm.removeControl('office_state');
      this.generatePdfForm.removeControl('office_pincode');
      this.generatePdfForm.removeControl('office_country_name');

    }
  }

  get f(){
    return this.generatePdfForm.controls;
  }


  onChangeJpg(event){
    this.selectedJpgFiles = event.target.files;
    const file = this.selectedJpgFiles.item(0);
    this.Jpgfilename= file.name;
  }

  onChangePdf(event){
    this.selectedPdfFiles = event.target.files;
    const file = this.selectedPdfFiles.item(0);
    this.Pdffilename= file.name;
  }

  onGenerate(){
    if(this.generatePdfForm.valid){
      this.generatePdfForm.value.form_type = this.selectedFormType;
      this.formsservice.generatePdf(this.generatePdfForm.value).subscribe(data => {
        console.log(data);
        saveAs(data);
      } , err => {
        console.log(err);
      })
    }
    // this.loading = true;
    // if(this.generatePdfForm.valid){
    //   this.formsservice.submitForm(this.generatePdfForm.value).subscribe( data => {
    //     this.generatePdfForm.reset();
    //     setTimeout(() => {
    //       this.router.navigate(['/users/listpdfs'] )
    //     }, 400);
    //   }, err => {
    //     console.log(err);
    //   })
    // }

  }



  onSubmit(){
      this.success = '';
      if(this.selectedPdfFiles && this.selectedJpgFiles){
        const Pdffile = this.selectedPdfFiles.item(0);
        this.uploadservice.fileUpload(Pdffile , 'Forms/pdf');
        const PdffileName = Pdffile.name;
        const Jpgfile = this.selectedJpgFiles.item(0);
        this.uploadservice.fileUpload(Jpgfile , 'Forms/jpg');
        const JpgfileName = Jpgfile.name;
        const body = {
          filename : PdffileName,
          jpgfilename :  JpgfileName
        }
        setTimeout(() => {
          this.formsservice.uploadForm(body).subscribe(data => {
            this.loading = false;
            this.removefiles();
            this.success = "Files Uploaded Successfully to Admin";
            this.socket.emit('refresh' , {})
          } , err => {
            console.log(err);
          })
        }, 400);
      }else{
        alert("Please select the required files to upload");
      }
  }

  removefiles(){
    this.selectedPdfFiles = '';
    this.selectedJpgFiles = '';
    this.Pdffilename = '';
    this.Jpgfilename = '';
  }

}
