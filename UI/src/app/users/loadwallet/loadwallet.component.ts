import { Component, OnInit , ViewChild , ElementRef } from '@angular/core';
import { WalletService} from '../../_services/wallet.service';
import {Router,ActivatedRoute} from '@angular/router';
import {UploadService} from '../../_services/upload.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {io} from 'socket.io-client';
import *as AOS from 'aos';
import { environment } from '@environments/environment';

@Component({
  selector: 'app-loadwallet',
  templateUrl: './loadwallet.component.html',
  styleUrls: ['./loadwallet.component.less']
})

export class LoadwalletComponent implements OnInit {
  filename = "No File Selected";
  loading = false;
  success = "";
  loadwalletform : FormGroup;
  selectedFiles : any;
  socket : any;

  @ViewChild('amountInput') amountinputRef : ElementRef;
  @ViewChild('referenceIdInput') refidinput : ElementRef;

  constructor(private wallet : WalletService,
            private router : Router , private snap : ActivatedRoute,
            private uploadservice : UploadService) { 
              this.socket = io(environment.socketUrl);
            }


  ngOnInit(): void {
      AOS.init({
        duration:1300
      })
    
    this.success = this.snap.snapshot.fragment;
    this.snap.fragment.subscribe(

      (fragment : string) => {
        this.success = fragment;
      }

    );

      this.loadwalletform = new FormGroup({
        "amount" : new FormControl(null,[Validators.required,Validators.pattern(/^[1-9][0-9]*$/)]),
        "referenceId" : new FormControl(null,Validators.required)
      })


  }
get f(){
  return this.loadwalletform.controls;
}

onChange(event){
  this.selectedFiles = event.target.files;
  const file = this.selectedFiles.item(0);
  this.filename = file.name;
}
onSubmit(){

  if(this.loadwalletform.valid){
    const file  = this.selectedFiles.item(0);
    this.uploadservice.fileUpload(file , "Wallets");
    const imagePath = file.name;
    this.loading = true;
    this.loadwalletform.value.imagepath = imagePath;
    setTimeout(() => {
      this.wallet.loadWallet(this.loadwalletform.value).subscribe( data => {
        this.loading = false;
        this.socket.emit('refresh' , {});
        this.router.navigate( ['/users/wallet'] , {fragment : 'success'} );
        this.loadwalletform.reset();
      } ,err => {
        console.log(err);
      })
    }, 400);
  
    setTimeout(() => {
      this.router.navigate( ['/users/wallethistory'] );
    }, 2000);


  }


}



}
