import { Component, OnInit , AfterViewInit ,ViewChild } from '@angular/core';
import{MatTableDataSource} from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import *as AOS from 'aos';
import { TokenService } from '@app/_services/token.service';
import { UsersService } from '@app/_services/users.service';
import {io} from 'socket.io-client';
import { environment } from '@environments/environment';
import { FormsService } from '@app/_services/forms.service';
import { UploadService } from '@app/_services/upload.service';
import { saveAs } from 'file-saver';
import{Buffer} from 'buffer';


@Component({
  selector: 'app-listpdf',
  templateUrl: './listpdf.component.html',
  styleUrls: ['./listpdf.component.less']
})
export class ListpdfComponent implements OnInit , AfterViewInit {

  details : any;
  data : any;
  formData : any = [];
  formsarray = [];
  socket : any;

  displayedColumns: string[] = ['fullname', 'uploadedAt','status' , 'statusUpdatedAt','reasons' , 'actions' ];

  @ViewChild(MatPaginator) paginator : MatPaginator;
  dataSource: MatTableDataSource<unknown>;
  constructor( private tokenService : TokenService,
              private userService : UsersService , private formService : FormsService , private uploadservice : UploadService) { 
                this.socket = io(`${environment.socketUrl}`);

              }

  ngOnInit(): void {
    this.data = this.tokenService.getPayload();
    AOS.init({
      duration : 1500
    })

    this.userService.GetUserById(this.data._id).subscribe( data => {
      this.formData = data.result.uploadedForms.reverse();
      for(let i=0;i< this.formData.length ; i++){
          this.formsarray.push(this.formData[i].uploadedFormId);
      }
      this.dataSource = new MatTableDataSource(this.formsarray);
    }, err => {
      console.log(err);
    })

    // socket io on add new form
    this.socket.on('refreshPage' , () => {
      this.userService.GetUserById(this.data._id).subscribe(data => {
        this.formsarray = [];
        this.formData = data.result.uploadedForms.reverse();
        for(let i=0;i< this.formData.length ; i++){
            this.formsarray.push(this.formData[i].uploadedFormId);
        }
        this.dataSource = new MatTableDataSource(this.formsarray);
        this.dataSource.paginator = this.paginator;
      } , err => {
        console.log(err);
      });
    })
   
  }

  async getFile(fileName : string){
    const file = await  this.uploadservice.getFile(fileName, "Acknowledgements");
    console.log(file.Body);
    var blob = new Blob([ this.encode(file.Body) ]);
    saveAs(blob,fileName);
  }
  encode(data){
    let buf = Buffer.from(data);
    let base64 = buf.toString('base64');
    return buf;
    }


  ngAfterViewInit(){
   setTimeout(() => {
    this.dataSource.paginator = this.paginator;
   }, 300);
  }

}
