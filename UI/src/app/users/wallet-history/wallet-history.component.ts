import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { TokenService } from '@app/_services/token.service';
import { UsersService } from '@app/_services/users.service';
import { WalletService } from '@app/_services/wallet.service';
import { environment } from '@environments/environment';
import * as AOS from 'aos';
import {io} from 'socket.io-client';


@Component({
  selector: 'app-wallet-history',
  templateUrl: './wallet-history.component.html',
  styleUrls: ['./wallet-history.component.less']
})
export class WalletHistoryComponent implements OnInit , AfterViewInit {

  data : any;
  walletsData = [];
  walletsArray = [];
  socket : any;

  displayedColumns : string[] = ['amount','referenceid','loadedAt','status','statusupdatedAt','reasonstoreject'];
  @ViewChild(MatPaginator) paginator : MatPaginator;
  dataSource : MatTableDataSource<any>;


  walletHistory : any;

  constructor(
              private wallet : WalletService , private tokenservice : TokenService , private userService : UsersService) { 
                this.socket = io(`${environment.socketUrl}`);
              }
  

  ngOnInit(): void {
    this.data = this.tokenservice.getPayload();
    AOS.init({
      duration : 1000
    })
    this.userService.GetUserById(this.data._id).subscribe(data => {
      this.walletsData = data.result.wallets.reverse();
      this.walletsArray = [];
      for(let i=0 ; i<this.walletsData.length ;i++){
        this.walletsArray.push(this.walletsData[i].walletId);
      }
      this.dataSource = new MatTableDataSource(this.walletsArray);
    } , err => {
      console.log(err);
    })

    this.socket.on('refreshPage' , () => {
      this.data = this.tokenservice.getPayload();
      this.userService.GetUserById(this.data._id).subscribe(data => {
        this.walletsData = data.result.wallets.reverse();
        this.walletsArray = [];
        for(let i=0 ; i<this.walletsData.length ;i++){
          this.walletsArray.push(this.walletsData[i].walletId);
        }
        this.dataSource = new MatTableDataSource(this.walletsArray);
        this.dataSource.paginator = this.paginator;
      } , err => {
        console.log(err);
      })

    })
    
  }
  

  ngAfterViewInit(){
    setTimeout(() => {
      this.dataSource.paginator = this.paginator;
    }, 300);
  }
  applyFilter(filterValue : string){
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


}
