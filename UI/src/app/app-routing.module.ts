import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home';
import { AdminGuard, AuthGuard, UserGuard } from './_helpers';
import{ListpdfComponent} from './users/listpdf/listpdf.component';
import{UsersComponent} from './users/users.component';
import{LoadwalletComponent} from './users/loadwallet/loadwallet.component';
import{SubmitformComponent} from './users/submitform/submitform.component';
import { WalletHistoryComponent } from './users/wallet-history/wallet-history.component';
import { ProfileComponent } from './profile/profile.component';
import { UpdateComponent } from './profile/update/update.component';
import { ContactUsComponent } from './contact-us/contact-us.component';

const accountModule = () => import('./account/account.module').then(x => x.AccountModule);
const adminModule = () => import('./admin/admin.module').then(x => x.AdminModule);

const routes: Routes = [
    { path: '', component: HomeComponent},
    { path: 'account', loadChildren: accountModule },
    {path : 'profile' , component : ProfileComponent , children : [
        {path : 'update' , component : UpdateComponent , canActivate : [AuthGuard]}
    ] , canActivate : [AuthGuard]},
    { path: 'admin', loadChildren: adminModule , canActivate : [AdminGuard]},
    {path : 'users/listpdfs',component : ListpdfComponent,  canActivate : [UserGuard]},
    {path : 'users/wallet',component:LoadwalletComponent,  canActivate : [UserGuard]},
    {path : 'users/submitform' , component : SubmitformComponent, canActivate : [UserGuard]},
    {path : 'users/wallethistory' , component : WalletHistoryComponent, canActivate : [UserGuard]},
    {path : 'contact-us' , component : ContactUsComponent},
    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
