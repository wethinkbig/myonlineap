﻿import { Component, OnInit } from '@angular/core';

import {TokenService} from '../_services/token.service';

@Component({ templateUrl: 'home.component.html' })
export class HomeComponent implements OnInit {

    data : any;
    constructor( private tokenservice : TokenService) { }
    ngOnInit(){
    }
}
