﻿import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { MustMatch } from '@app/_helpers';
import { AuthService } from '@app/_services/auth.service';

enum TokenStatus {
    Validating,
    Valid,
    Invalid
}

@Component({ templateUrl: 'reset-password.component.html' })
export class ResetPasswordComponent implements OnInit {
    token = null;
    form: FormGroup;
    submitted = false;
    error : string;
    success : string;

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private authService : AuthService
    ) { }

    ngOnInit() {
        this.form = this.formBuilder.group({
            password: ['', [Validators.required, Validators.minLength(6)]],
            confirmPassword: ['', Validators.required],
        }, {
            validator: MustMatch('password', 'confirmPassword')
        });

        this.route.params.subscribe(params => {
            this.token = params.id;
        })


    }

    // convenience getter for easy access to form fields
    get f() { return this.form.controls; }

    onSubmit() {
        this.submitted = true;
        if(this.form.valid){
            this.form.value.link = this.token;
            const data = {
                link : this.token,
                newpass : this.form.value.password
            }
            this.authService.resetpassword(data).subscribe(data => {
                console.log(data);
                 this.success = 'Password had been reset succesfully now you can login from login page';
                this.form.reset();
            } , err=> {
                this.error = err;
            })
        }
       
    }
}