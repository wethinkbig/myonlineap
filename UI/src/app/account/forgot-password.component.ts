﻿import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first, finalize } from 'rxjs/operators';
import { AuthService } from '@app/_services/auth.service';

@Component({ templateUrl: 'forgot-password.component.html' })
export class ForgotPasswordComponent implements OnInit {
    form: FormGroup;
    submitted = false;
    error : string;
    success : string;

    constructor(
        private formBuilder: FormBuilder,
        private authService : AuthService
    ) { }

    ngOnInit() {
        this.form = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]]
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.form.controls; }

    onSubmit() {
        this.submitted = true;
        if(this.form.valid){
            this.authService.forgotpassword(this.form.value).subscribe(data => {
                console.log(data);
                this.success = "A password link has been sent to your email-id . Please click on the link to reset your password !"
            } , err => {
                console.log(err);
                this.error = err;                
            })
        }
       
    }
}