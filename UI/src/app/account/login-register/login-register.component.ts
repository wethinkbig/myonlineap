import { Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { TokenService } from '@app/_services/token.service';
import { environment } from '@environments/environment';
import * as AOS from 'aos';
import {io} from 'socket.io-client';


@Component({
  selector: 'app-login-register',
  templateUrl: './login-register.component.html',
  styleUrls: ['./login-register.component.less']
})
export class LoginRegisterComponent implements OnInit {
  data : any;
  socket : any;
  constructor(private tokenService : TokenService,
              private router : Router) {
                this.socket = io(environment.socketUrl);
               }

  ngOnInit(): void {
    this.socket.emit('createDefaultAdmin' , {});
    AOS.init({
      duration : 1000,
      disable : function() {
        var maxWidth = 768;
        return window.innerWidth < maxWidth;
      }
    });

    this.data = this.tokenService.getPayload();
    if(this.data){
      this.router.navigate(['/home']);
    }

  }

}
