import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { BehaviorSubject } from 'rxjs';
import {AuthService} from '../../../_services/auth.service';
import {TokenService} from '../../../_services/token.service';
import {io} from 'socket.io-client';
import { environment } from '@environments/environment';


@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.less']
})
export class SignInComponent implements OnInit {
    form: FormGroup;
    loading = false;
    submitted = false;
    error = null;
    socket : any;
    amount : number;

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private authservice : AuthService,
        private tokenService : TokenService
    ) { 
        this.socket = io(`${environment.socketUrl}`);
    }

    ngOnInit() {
        this.form = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required]
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.form.controls; }

    onSubmit() { 
        if(this.form.valid){
            this.authservice.loginUser(this.form.value).subscribe(data => {
                // console.log(data);
                this.socket.emit('refresh' , {});
                this.tokenService.setToken(data.token);
                this.form.reset();
                this.router.navigate(['/profile']);
            }, err => {
                this.error = err.error.message;   
            })
        }
      
    }
}
