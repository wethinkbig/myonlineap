import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {io} from 'socket.io-client';
import {AuthService} from '../../../_services/auth.service';
import { MustMatch } from '@app/_helpers';
import { TokenService } from '@app/_services/token.service';
import { environment } from '@environments/environment';
declare var $:any;


@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.less']
})
export class SignUpComponent implements OnInit {

  form: FormGroup;
    loading = false;
    submitted = false;
    titles =  ["Mr","Mrs","Miss","Ms"];
    error = null;
    socket : any;

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private authService : AuthService,
        private tokenService : TokenService,
    ) { 
      this.socket = io(`${environment.socketUrl}`);
    }

    ngOnInit() {
        this.form = this.formBuilder.group({
            title: ['', Validators.required],
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.minLength(6)]],
            confirmpassword: ['', Validators.required]
        }, {
            validator: MustMatch('password', 'confirmpassword')
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.form.controls; }

    onSubmit() {
        if(this.form.valid){
            this.form.value.amount = 0;
            this.form.value.role = "User";
            this.authService.registerUser(this.form.value).subscribe(data => {
                console.log(data);
                this.form.reset();
                this.socket.emit('refresh' , {});
                this.tokenService.setToken(data.token);
                this.router.navigate(['/profile']);
            } , err => {
                this.error = err;
            })
        }
      
    }

}
