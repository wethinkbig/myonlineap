import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { AccountRoutingModule } from './account-routing.module';
import { LayoutComponent } from './layout.component';
import { ForgotPasswordComponent } from './forgot-password.component';
import { ResetPasswordComponent } from './reset-password.component';
import { LoginRegisterComponent } from './login-register/login-register.component';
import { MaterialDesignModule } from '../material.module';
import { SignInComponent } from './login-register/sign-in/sign-in.component';
import { SignUpComponent } from './login-register/sign-up/sign-up.component';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        AccountRoutingModule,
        MaterialDesignModule
    ],
    declarations: [
        LayoutComponent,
        ForgotPasswordComponent,
        ResetPasswordComponent,
        LoginRegisterComponent,
        SignInComponent,
        SignUpComponent,
    ]
})
export class AccountModule { }