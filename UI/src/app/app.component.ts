﻿import { Component , OnInit } from '@angular/core';
import * as M from '../assets/materialize/js/materialize.js';
import { Router } from '@angular/router';
import { TokenService } from './_services/token.service';
import { UsersService } from './_services/users.service.js';
import {io} from 'socket.io-client';
import { environment } from '@environments/environment.js';


@Component({ selector: 'app', templateUrl: 'app.component.html' , styleUrls : ['./app.component.less'] })
export class AppComponent implements OnInit {
    account: Account;
    fetchedamount : number;

    data : any;
    userData : any;
    socket : any;

    constructor( 
                private router : Router , private tokenService : TokenService,
                private userService : UsersService) {
            this.socket = io(environment.socketUrl);
    }
    ngOnInit(){
        
    }
   
}