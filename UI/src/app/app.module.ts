﻿import { NgModule, APP_INITIALIZER } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

// used to create fake backend
// generati service
import { WalletService } from './_services/wallet.service';
import { FormsService } from './_services/forms.service';
import { UploadService } from './_services/upload.service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home';
import { ListpdfComponent } from './users/listpdf/listpdf.component';
import { UsersComponent } from './users/users.component';
import { LoadwalletComponent } from './users/loadwallet/loadwallet.component';
import { SubmitformComponent } from './users/submitform/submitform.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import{MaterialDesignModule} from './material.module';
import { WalletHistoryComponent } from './users/wallet-history/wallet-history.component';
import { DetailsComponent } from './profile/details/details.component';
import { UpdateComponent } from './profile/update/update.component'
import { ProfileComponent } from './profile/profile.component';;
import { ContactUsComponent } from './contact-us/contact-us.component'
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';;
import { NavigationComponent } from './navigation/navigation.component'
;
import { LayoutModule } from '@angular/cdk/layout'
;
import { MatToolbarModule } from '@angular/material/toolbar'
;
import { MatButtonModule } from '@angular/material/button'
;
import { MatSidenavModule } from '@angular/material/sidenav'
;
import { MatIconModule } from '@angular/material/icon'
;
import { MatListModule } from '@angular/material/list';;
import { RejectmodalComponent } from './rejectmodal/rejectmodal.component'
;
import { CookieService } from 'ngx-cookie-service';
import {TokenInterceptor} from './_helpers/token-interceptor';
import {DpDatePickerModule} from 'ng2-date-picker';
@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        ReactiveFormsModule,
        HttpClientModule,
        AppRoutingModule,
        FormsModule,
        BrowserAnimationsModule,
        MaterialDesignModule,
        ReactiveFormsModule,
        FlexLayoutModule,DpDatePickerModule,
        LayoutModule,
        MatToolbarModule ,
        MatButtonModule ,
        MatSidenavModule ,
        MatIconModule ,
        MatListModule
],
    declarations: [
        AppComponent,
        HomeComponent,
        ListpdfComponent,
        UsersComponent,
        LoadwalletComponent,
        SubmitformComponent,
        WalletHistoryComponent,
        DetailsComponent,
        UpdateComponent  , ProfileComponent, ContactUsComponent , NavigationComponent, RejectmodalComponent
],
    providers: [
        CookieService,
        {provide : HTTP_INTERCEPTORS , useClass : TokenInterceptor , multi : true},
         WalletService, FormsService, UploadService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
