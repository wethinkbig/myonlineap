import { Component, OnInit } from '@angular/core';
import { TokenService } from '@app/_services/token.service';
import { UsersService } from '@app/_services/users.service';
import * as AOS from 'aos';


@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.less']
})
export class DetailsComponent implements OnInit {

  data : any;
  userData  : any = {};

  constructor( private tokenservice : TokenService,
              private userService : UsersService) { }

  ngOnInit(): void {
    this.data = this.tokenservice.getPayload();
    AOS.init({
      duration : 1000,
      disable : function() {
        var maxWidth = 768;
        return window.innerWidth < maxWidth;
      }
    });
    this.getUser();
  }

  getUser(){
    this.userService.GetUserById(this.data._id).subscribe(data => {
      this.userData = data.result;
      
    }, err => {
      console.log(err);
      
    })
  }
  

}
