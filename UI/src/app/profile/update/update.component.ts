import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import * as AOS from 'aos';
import { MustMatch } from '@app/_helpers';
import { TokenService } from '@app/_services/token.service';
import { UsersService } from '@app/_services/users.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.less']
})
export class UpdateComponent implements OnInit  {

  form: FormGroup;
  loading = false;
  submitted = false;
  deleting = false;
  data : any;


  constructor(
      private formBuilder: FormBuilder,
      private route: ActivatedRoute,
      private router: Router,
      private tokenservice : TokenService,
      private userService : UsersService
  ) { }

  ngOnInit() {
    this.data = this.tokenservice.getPayload();
    this.getUser();
    AOS.init({
        duration : 1000,
        disable : function() {
          var maxWidth = 768;
          return window.innerWidth < maxWidth;
        }
      });
      this.form = this.formBuilder.group({
          title: [this.data.title, Validators.required],
          firstName: [this.data.firstname, Validators.required],
          lastName: [this.data.lastname, Validators.required],
          email: [this.data.email, [Validators.required, Validators.email]],
          password: ['', [Validators.minLength(6)]],
          confirmPassword: ['']
      }, {
          validator: MustMatch('password', 'confirmPassword')
      });
  }
  getUser(){
      this.userService.GetUserById(this.data._id).subscribe(data => {
        //   this.userData = data.result;
      }, err=> {
          console.log(err);
      })
      
  }

  // convenience getter for easy access to form fields
  get f() { return this.form.controls; }

  onSubmit() {

        if(this.form.valid){
            this.userService.updateUser(this.data._id , this.form.value).subscribe(data => {
                alert('Your account details had been updated..Plz login again');
                this.tokenservice.deleteToken();
                this.router.navigate(['/']);
            } , err => {
                console.log(err);
                
            })
            
        }

    
  }

  onDelete() {
      if (confirm('Are you sure?')) {
          this.userService.deleteUser(this.data._id).subscribe( data => {
              this.tokenservice.deleteToken();
              this.router.navigate(['/']);
          }, err => {
              console.log(err);
          })
       
      }
  }

}
