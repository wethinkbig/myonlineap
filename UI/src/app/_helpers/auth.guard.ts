﻿import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';

import { TokenService } from '@app/_services/token.service';
import { Observable } from 'rxjs';

@Injectable({
    providedIn : 'root'
})
export class AuthGuard implements CanActivate{
    constructor(private router : Router , private tokenserive : TokenService){}

    canActivate(
        route : ActivatedRouteSnapshot , state : RouterStateSnapshot
    ) : Observable<boolean> | Promise <boolean> | boolean{
        const token = this.tokenserive.getPayload();
        if(token){
             return true;
        }
        this.router.navigate(['/home']);
        return false
    }
}
@Injectable({
    providedIn : 'root'
})
export class UserGuard implements CanActivate{
    constructor(private router : Router , private tokenservice : TokenService){}
    canActivate(router : ActivatedRouteSnapshot , state : RouterStateSnapshot):Observable<boolean> | Promise<boolean> | boolean{
        const token = this.tokenservice.getPayload();
        if(token){
            if(token.role === 'User'){
                return true;
            }
          this.router.navigate(['/home']);
            return false;
        }
        this.router.navigate(['/home']);
        return false;
    }
}

@Injectable({
    providedIn : 'root'
})
export class AdminGuard implements CanActivate{
    constructor(private router : Router , private tokenservice : TokenService){}
    canActivate(router : ActivatedRouteSnapshot , state : RouterStateSnapshot):Observable<boolean> | Promise<boolean> | boolean{
        const token = this.tokenservice.getPayload();
        if(token){
            if(token.role === 'Admin'){
                return true;
            }
          this.router.navigate(['/home']);
            return false;
        }
        this.router.navigate(['/home']);
        return false;
    }
}