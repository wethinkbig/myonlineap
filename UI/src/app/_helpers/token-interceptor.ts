import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { TokenService } from "../_services/token.service";

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

    constructor(private tokenservice : TokenService){}

    intercept(req : HttpRequest<any> , next : HttpHandler) : Observable<HttpEvent<any>>{
        const headersConfig  = {
            'Content-Type' :'application/json',
            'Accept' : 'application/json'
        };
        const token = this.tokenservice.getToken();
        if(token){
            headersConfig['authorization'] = `beader ${token}`;
        }
        const _request = req.clone({setHeaders :headersConfig});

        return next.handle(_request);
    }
}
