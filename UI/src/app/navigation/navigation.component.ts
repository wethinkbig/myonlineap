import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import {TokenService} from '../_services/token.service';
import {UsersService} from '../_services/users.service';
import {io} from 'socket.io-client';
import { TranslationWidth } from '@angular/common';
import { Router } from '@angular/router';
import { environment } from '@environments/environment';



@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.less']
})
export class NavigationComponent implements OnInit{

  data : any;
  socket : any;
  userData : any;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );
    constructor( private breakpointObserver : BreakpointObserver,
      private tokenservice : TokenService , private userService : UsersService , private router : Router) {
        this.socket = io(`${environment.socketUrl}`);
    }
    ngOnInit(){
      this.getToken();
      this.socket.on('refreshPage' ,() => {
        this.getToken();
        this.getData();
      })
    }

    getToken(){
      let token = this.tokenservice.getToken();
      if(token){
        this.data = this.tokenservice.getPayload();
        this.getData();
      }else{
        this.userData = null;
      }
    }

    getData(){
        this.userService.GetUserById(this.data._id).subscribe(data => {
          this.userData = data.result;
        } , err => {
          console.log(err);
          if(err.error.token === null){
            this.tokenservice.deleteToken();
            this.router.navigate(['/']);
          }
         
        })
    }

    logout() {
        this.tokenservice.deleteToken();
        this.router.navigate(['/']);
        this.socket.emit('refresh' , {});
    }


}
