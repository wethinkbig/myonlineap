const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const cookieparser = require('cookie-parser');
const dbconfig = require('./config/secrets');
const auth = require('./routes/authRoutes');
const users = require('./routes/userRoutes');
const forms = require('./routes/formRoutes');
const wallets = require('./routes/walletRoutes');

const app = express();

app.use(cors());
app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ extended: true, limit: '50mb' }));
app.use('/api/onlineap', auth);
app.use('/api/onlineap', users);
app.use('/api/onlineap', forms);
app.use('/api/onlineap', wallets);


const http = require('http').Server(app);
const io = require('socket.io')(http, {
    cors: {
        origin: '*'
    }
});

mongoose.Promise = global.Promise;
mongoose.connect(dbconfig.url, { useNewUrlParser: true, useUnifiedTopology: true });

require('./sockets/streams')(io);

http.listen(3000, () => {
    console.log('server listening on port number 3000');
})
