const user = require('../models/userModels');
const httpstatus = require('http-status-codes');
const helper = require('../helpers/helpers');
const bcrypt = require('bcryptjs');

module.exports = {

    async GetAllUsers(req, res) {
        await user.find({
            role: { $ne: 'Admin' }
        }).populate('forms.formId').populate('wallets.walletId').then((result) => {
            res.status(httpstatus.OK).json({ message: 'All users', result });
        }).catch(err => {
            res.status(httpstatus.INTERNAL_SERVER_ERROR).json({ message: 'An internal error occured' });
        })
    },

    async GetUser(req, res) {
        user.findOne({
            _id: req.params.id
        }).populate('forms.formId').populate('uploadedForms.uploadedFormId').populate('wallets.walletId').then((result) => {
            res.status(httpstatus.OK).json({ message: 'users by id', result });
        }).catch(err => {
            res.status(httpstatus.INTERNAL_SERVER_ERROR).json({ message: 'An internal error occured' });
        })
    },

    async GetUserByName(req, res) {
        user.findOne({
            username: req.params.username,
            role: { $ne: 'Admin' }
        }).populate('forms.formId').populate('wallets.walletId').then((result) => {
            res.status(httpstatus.OK).json({ message: 'user by name', result });
        }).catch(err => {
            res.status(httpstatus.INTERNAL_SERVER_ERROR).json({ message: 'An internal error occured', err });
        })
    },

    async updateUser(req, res) {
        const id = req.params.id;
        const userData = await user.findOne({
            _id: id
        })
        if (userData) {
            if (req.body.password === '') {
                passwordfetch = userData.password;
            } else {
                passwordfetch = await bcrypt.hash(req.body.password, 10);
            }

            user.update({
                _id: req.params.id
            }, {
                $set: {
                    title: req.body.title,
                    firstname: helper.capitalize(req.body.firstName),
                    lastname: helper.capitalize(req.body.lastName),
                    email: req.body.email,
                    password: passwordfetch
                }
            }).then((result) => {
                res.status(httpstatus.OK).json({ message: 'User updated success' })
            }).catch(err => {
                res.status(httpstatus.INTERNAL_SERVER_ERROR).json({ message: 'An internal server occured', err });
            })

        }

    },

    async deleteUser(req, res) {
        const id = req.params.id;
        user.deleteOne({ _id: id }).then((result) => {
            res.status(httpstatus.OK).json({ message: 'User deleted successfully' })
        }).catch(err => {
            res.status(httpstatus.INTERNAL_SERVER_ERROR).json({ message: 'An internal server occured', err });
        })
    }
}