const bcrypt = require('bcryptjs');
const httpstatus = require('http-status-codes');
const helper = require('../helpers/helpers');
const jwt = require('jsonwebtoken');
const dbconfig = require('../config/secrets');

const Form = require('../models/formModels');
const User = require('../models/userModels');
const Uploadform = require('../models/uploadFormModels');



module.exports = {


    async Submitform(req, res) {

        const body = {
            user: req.user._id,
            firstname: req.body.firstName,
            lastname: req.body.lastName,
            cityname: req.body.cityName,
            gender: req.body.gender,
            createdAt: new Date()
        }
        Form.create(body).then(async (form) => {
            await User.update({
                _id: req.user._id
            }, {
                $push: {
                    forms: {
                        formId: form._id,
                        createdAt: new Date()
                    }
                }
            })

            res.status(httpstatus.OK).json({ message: 'Form added successfully', form });
        }).catch(err => {
            res.status(httpstatus.INTERNAL_SERVER_ERROR).json({ message: 'Something error occured', err });
        })

    },

    async Uploadform(req, res) {

        const body = {
            user: req.user._id,
            fullname : helper.capitalize(req.user.firstname+ " "+req.user.lastname),
            email : helper.lowercase(req.user.email),
            uploadedAt: new Date(),
            filename: req.body.filename,
            jpgfilename : req.body.jpgfilename
        }
        Uploadform.create(body).then(async (uploadform) => {
            await User.update({
                _id: req.user._id
            }, {
                $push: {
                    uploadedForms: {
                        uploadedFormId: uploadform._id
                    }
                }
            })
            res.status(httpstatus.OK).json({ message: 'Form Uploaded successfully', uploadform });
        }).catch(err => {
            res.status(httpstatus.INTERNAL_SERVER_ERROR).json({ message: 'Something error occured', err });
        })



    },
    async getUploadedForms(req, res) {
        try {
            const uploadedforms = await Uploadform.find({})
                .populate('user')
                .sort({ uploadedAt: -1 });

            return res.status(httpstatus.OK).json({ message: 'All uploaded forms', uploadedforms });
        } catch (err) {
            return res.status(httpstatus.INTERNAL_SERVER_ERROR).json({ message: 'Error occured' });
        }
    },

    async updateFormStatus(req, res) {

        const uploadedFormId = req.body.id;
        const status = req.body.status;
        await Uploadform.update({
            _id: uploadedFormId
        }, {
            $set: {
                status: status,
                reasons : req.body.reasons,
                statusUpdatedAt : new Date(),
                approvedfile : req.body.filename
            }
        }).then(() => {
            res.status(httpstatus.OK).json({ message: 'status updated succesfully to' });
        }).catch(err => {
            return res.status(httpstatus.INTERNAL_SERVER_ERROR).json({ message: 'Error occured' });
        })

    }



}