const bcrypt = require('bcryptjs');
const httpstatus = require('http-status-codes');
const helper = require('../helpers/helpers');
const jwt = require('jsonwebtoken');
const dbconfig = require('../config/secrets');

const Wallet = require('../models/walletModels');
const User = require('../models/userModels');

module.exports = {
    async loadWallet(req, res) {
        const body = {
            user: req.user._id,
            amount: req.body.amount,
            imagepath: req.body.imagepath,
            referenceid: req.body.referenceId,
            email: req.user.email,
            loadedAt: new Date()
        }
        Wallet.create(body).then(async (wallet) => {
            await User.update({
                _id: req.user._id
            }, {
                $push: {
                    wallets: {
                        walletId: wallet._id
                    }
                }
            })
            console.log('asds');
            res.status(httpstatus.OK).json({ message: 'Wallet loaded successfully', wallet });
        }).catch(err => {
            res.status(httpstatus.INTERNAL_SERVER_ERROR).json({ message: 'Something error occured', err });
        })
    },

    async getWallets(req, res) {
        await Wallet.find({}).populate('user').sort({ loadedAt: -1 }).then((result) => {
            res.status(httpstatus.OK).json({ message: 'All wallets history', result });
        }).catch(err => {
            res.status(httpstatus.INTERNAL_SERVER_ERROR).json({ message: 'Something error occured', err });
        })
    },

    async updateWallet(req, res) {

        if (req.body.status === 'Approved') {

            const updating = async () => {
                await Wallet.update({
                    _id: req.body.id
                }, {
                    $set: {
                        status: req.body.status,
                        statusupdatedAt: new Date(),
                    }
                })

                user = await User.findOne({ _id: req.body.userid });

                addamount = user.amount + parseInt(req.body.amount);

                await User.update({
                    _id: req.body.userid
                }, {
                    $set: {
                        amount: addamount
                    }
                })
            }
            updating().then(() => {
                res.status(httpstatus.OK).json({ message: 'wallet had been successfulyy updated' });
            }).catch(err => {
                res.status(httpstatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal error occured' });
            })

        } else {
            await Wallet.update({
                _id: req.body.id
            }, {
                $set: {
                    status: req.body.status,
                    statusupdatedAt: new Date(),
                    reasonstoreject: req.body.reasons
                }
            }).then(() => {
                res.status(httpstatus.OK).json({ message: 'wallet had been successfulyy rejected' });
            }).catch(err => {
                res.status(httpstatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal error occured' });
            })
        }



    }
}