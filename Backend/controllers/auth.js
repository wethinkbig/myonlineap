const joi = require('joi');
const jwt = require('jsonwebtoken');
const user = require('../models/userModels');
const bcrypt = require('bcryptjs');
const helper = require('../helpers/helpers');
const dbconfig = require('../config/secrets');
const httpstatus = require('http-status-codes');
const nodemailer = require('nodemailer');

module.exports = {

    // create default admin method
    async registerDefaultAdmin() {

        const email = await user.findOne({
            email: helper.lowercase('admin@myonlineap.com')
        })
        if (!email) {
            return bcrypt.hash('admin', 10, (err, hash) => {
            
                const body = {
                    firstname: helper.capitalize('admin'),
                    lastname: helper.capitalize('myonlineap'),
                    title: 'Mr',
                    email: helper.lowercase('admin@myonlineap.com'),
                    password: hash,
                    createdAt: new Date(),
                    role: 'Admin'
                };
                user.create(body)
    
            })
        }
        
    },



    async registerUser(req, res) {

        const email = await user.findOne({
            email: helper.lowercase(req.body.email)
        })
        if (email) {
            return res.status(httpstatus.CONFLICT).json({ message: 'User already exists with given email' })
        }
        return bcrypt.hash(req.body.password, 10, (err, hash) => {
            if (err) {
                return res.status(httpstatus.BAD_REQUEST).json({ message: 'Error occured in hashing your password' });
            }
            const body = {
                firstname: helper.capitalize(req.body.firstName),
                lastname: helper.capitalize(req.body.lastName),
                amount: req.body.amount,
                title: req.body.title,
                email: helper.lowercase(req.body.email),
                password: hash,
                createdAt: new Date(),
                role: req.body.role,
            };

            user.create(body).then((user) => {
                const token = jwt.sign({ data: user }, dbconfig.secrets, {
                    expiresIn: '1h'
                });
                res.cookie('auth', token);
                res.status(httpstatus.CREATED).json({ message: 'User created successfully ', user, token });
            }).catch(err => {
                res.status(httpstatus.INTERNAL_SERVER_ERROR).json({ message: 'AN internal error occured' });
            })


        })
    },


    async loginUser(req, res) {
        if (!req.body.email || !req.body.password) {
            return res.status(httpstatus.CONFLICT).json({ message: 'No empty fields required' });
        }
        const userData = await user.findOne({
            email: helper.lowercase(req.body.email)
        })
        if (!userData) {
            return res.status(httpstatus.NOT_FOUND).json({ message: 'Username not found ! 😪try again' });
        }
        return bcrypt.compare(req.body.password, userData.password).then((result) => {
            if (!result) {
                res.status(httpstatus.NOT_FOUND).json({ message: 'Password is incorrect' });
            }else{
                const token = jwt.sign({ data: userData }, dbconfig.secrets, {
                    expiresIn: '1h'
                });
                res.cookie('auth', token);
                res.status(httpstatus.OK).json({ message: 'Login successfull', userData, token })
            }
           
        }).catch(err => {
            console.log(err,"err")
            res.status(httpstatus.INTERNAL_SERVER_ERROR).json({ message: 'An internal error occured' });
        })



    },

    async forgotPassword(req, res) {

        var transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: 'sunil.pandvd22@gmail.com',
                pass: '9959907940s'
            }
        });

        const userData = await user.findOne({
            email: req.body.email,
        })
        if (!userData) {
            return res.status(httpstatus.NOT_FOUND).json({ message: 'Username with this email ! does not exits !' });
        }
        const token = jwt.sign({ _id: userData._id }, dbconfig.secrets, { expiresIn: '20m' });
        const mailoptions = {
            from: 'sunil.pandvd22@gmail.com',
            to: req.body.email,
            subject: 'Password Reset Link',
            html: `
                    <h2>Please click on the given link to reset your password</h2>
                    <p>http://localhost:4200/account/reset-password/${token}</p>

            `
        };

        user.update({
            email: req.body.email
        }, {
            $set: {
                resetlink: token
            }
        }).catch(err => {
            return res.status(httpstatus.NOT_FOUND).json({ message: 'Password reset link error' });
        })

        transporter.sendMail(mailoptions, function (error, info) {
            if (error) {
                return res.status(httpstatus.NOT_FOUND).json({ message: 'error', error });
            } else {
                return res.status(httpstatus.OK).json({ message: 'email sent successfully' });
            }

        })



    },
    resetPassword(req, res) {
        const { link, newpass } = req.body;
        if (link) {
            jwt.verify(link, dbconfig.secrets, (err, decoded) => {
                if (err) {
                    return res.status(httpstatus.CONFLICT).json({ message: 'Incorrect token or the link had expired' });
                }
            });

            bcrypt.hash(newpass, 10, (err, hash) => {
                if (err) {
                    return res.status(httpstatus.BAD_REQUEST).json({ message: 'Error occured in hasing your password' });
                }
                user.updateOne({
                    resetlink: link
                }, {
                    $set: {
                        password: hash
                    }
                }).then(() => {
                    res.status(httpstatus.OK).json({ successMessage: 'Password reset was successful and now you can login' });
                }).catch(err => {
                    return res.status(httpstatus.INTERNAL_SERVER_ERROR).json({ message: 'Error occured' });
                })
            })

        } else {
            return res.status(httpstatus.INTERNAL_SERVER_ERROR).json({ message: 'Authentication error!!!' });
        }
    },

    async sendMail(req, res) {
        var transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: 'sunil.pandvd22@gmail.com',
                pass: '9959907940s'
            }
        });
        const Tomailoptions = {
            from: 'no-reply@gmail.com',
            to: req.body.email,
            subject: 'Query / Feedback',
            html: `
            <h2>Thank you for contacting us => from ONLINEAP🥰</h2>
            <h2>We will shortly approach you </h2>
          `
        }
        const Selfmailoptions = {
            from: 'no-reply@gmail.com',
            to: 'sunil.pandvd22@gmail.com',
            subject: 'Query / Feedback',
            html: `
                <h2>From Name : ${req.body.fullname}</h2>
                <h2>From Email : ${req.body.email}</h2>
                <h2>Message : ${req.body.query}</h2>

            `
        }

        const sendmail = async () => {
            await transporter.sendMail(Tomailoptions);
            await transporter.sendMail(Selfmailoptions);
        }
        const result = req.body.email;
        sendmail().then(() => {
            res.status(httpstatus.OK).json({ message: 'mail has been sent successfully', result });
        }).catch(err => {
            res.status(httpstatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal error occured' });
        })

    }

}