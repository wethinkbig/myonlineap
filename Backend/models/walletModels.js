const mongoose = require('mongoose')

const Walletschema = mongoose.Schema({

    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    amount: { type: Number },
    imagepath: { type: String },
    referenceid: { type: String },
    status: { type: String, default: 'Pending' },
    loadedAt: { type: Date, default: Date.now() },
    statusupdatedAt: { type: Date, default: '' },
    reasonstoreject: { type: String, default: '' },
    email: { type: String }

})

module.exports = mongoose.model('Wallet', Walletschema)