const mongoose = require('mongoose')

const formSchema = mongoose.Schema({

    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    firstname: { type: String },
    lastname: { type: String },
    cityname: { type: String },
    gender: { type: String },
    createdAt: { type: Date, default: Date.now() }

})

module.exports = mongoose.model('Form', formSchema)