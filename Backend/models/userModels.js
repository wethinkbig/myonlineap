const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    firstname: { type: String },
    lastname: { type: String },
    amount: { type: Number },
    title: { type: String },
    email: { type: String },
    password: { type: String },
    createdat: { type: Date, default: Date.now() },
    isVerified: { type: String, default: true },
    role: { type: String },
    resetlink:
    {
        data: String,
        default: ''
    },
    forms: [
        {
            formId: { type: mongoose.Schema.Types.ObjectId, ref: 'Form' },
            createdAt: { type: Date, default: Date.now() }
        }
    ],

    uploadedForms: [
        {
            uploadedFormId: { type: mongoose.Schema.Types.ObjectId, ref: 'Uploadform' }
        }
    ],
    wallets: [
        {
            walletId: { type: mongoose.Schema.Types.ObjectId, ref: 'Wallet' }
        }
    ]

});

module.exports = mongoose.model('User', userSchema)