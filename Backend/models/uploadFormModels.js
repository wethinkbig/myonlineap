const mongoose = require('mongoose')

const uploadFormSchema = mongoose.Schema({

    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    fullname : {type : String},
    email : {type : String},
    uploadedAt: { type: Date, default: Date.now() },
    statusUpdatedAt : {type : Date },
    jpgfilename : {type : String , default : ''},
    filename: { type: String },
    approvedfile : {type : String , default : ''},
    status: { type: String, default: 'Pending' },
    reasons : {type : String , default : ''}

})

module.exports = mongoose.model('Uploadform', uploadFormSchema)