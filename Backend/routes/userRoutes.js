const express = require('express');
const routes = express.Router();

const AuthHelper = require('../helpers/AuthHelpers');
const userCtrl = require('../controllers/users');

routes.get('/users', AuthHelper.VerifyToken, userCtrl.GetAllUsers);
routes.get('/user/:id', AuthHelper.VerifyToken, userCtrl.GetUser);
routes.get('/username/:username', AuthHelper.VerifyToken, userCtrl.GetUserByName);
routes.put('/update/:id', AuthHelper.VerifyToken, userCtrl.updateUser);
routes.delete('/delete/:id', AuthHelper.VerifyToken, userCtrl.deleteUser);


module.exports = routes;