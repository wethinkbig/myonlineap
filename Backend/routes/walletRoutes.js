const express = require('express');
const routes = express.Router();

const AuthCtrl = require('../helpers/AuthHelpers');

const formCtrl = require('../controllers/wallets');

routes.post('/loadwallet', AuthCtrl.VerifyToken, formCtrl.loadWallet);
routes.get('/getwallets', AuthCtrl.VerifyToken, formCtrl.getWallets);
routes.get('/getwallets', AuthCtrl.VerifyToken, formCtrl.getWallets);
routes.put('/updatewallet', AuthCtrl.VerifyToken, formCtrl.updateWallet);




module.exports = routes;