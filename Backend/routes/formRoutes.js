const express = require('express');
const routes = express.Router();

const AuthCtrl = require('../helpers/AuthHelpers');

const formCtrl = require('../controllers/forms');

routes.post('/submitform', AuthCtrl.VerifyToken, formCtrl.Submitform);
routes.post('/uploadform', AuthCtrl.VerifyToken, formCtrl.Uploadform);
routes.get('/getUploadedForms', AuthCtrl.VerifyToken, formCtrl.getUploadedForms);
routes.put('/updateFormStatus', AuthCtrl.VerifyToken, formCtrl.updateFormStatus);




module.exports = routes;