const express = require('express');
const router = express.Router();

const AuthCtrl = require('../controllers/auth');

router.post('/login', AuthCtrl.loginUser);
router.post('/register', AuthCtrl.registerUser);
router.put('/forgot-password', AuthCtrl.forgotPassword);
router.put('/reset-password', AuthCtrl.resetPassword);
router.post('/sendmail', AuthCtrl.sendMail);




module.exports = router;