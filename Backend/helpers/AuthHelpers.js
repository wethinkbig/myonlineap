const jwt = require('jsonwebtoken');
const httpstatus = require('http-status-codes');
const dbconfig = require('../config/secrets');

module.exports = {
    VerifyToken: (req, res, next) => {
        if (!req.headers.authorization) {
            return res.status(httpstatus.UNAUTHORIZED).json({ message: 'Unauthorized' });
        }
        const token = req.headers.authorization.split(' ')[1];
        if (!token) {
            return res.status(httpstatus.FORBIDDEN).json({ message: 'No token provided' })
        }
        return jwt.verify(token, dbconfig.secrets, (err, decoded) => {
            if (err) {
                if (err.expiredAt < new Date()) {
                    return res.status(httpstatus.INTERNAL_SERVER_ERROR).json({ message: 'Token has expired please login again', token: null })
                }
                next();
            }

            req.user = decoded.data;
            next();
        })
    }
}