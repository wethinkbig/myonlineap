const auth = require('../controllers/auth');

module.exports = function (io) {
    io.on('connection', socket => {
        socket.on('refresh', (data) => {
            io.emit('refreshPage', {});
        })
        socket.on('createDefaultAdmin' , (data) => {
            auth.registerDefaultAdmin();
        })
        console.log('Client connected to the socket io backend');
    })
}