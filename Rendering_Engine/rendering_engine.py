import cv2
import numpy as np
import boxdetect
from boxdetect import config

page_one_rects = {
    "original_Shri": 531,
    "original_Smt":530,
    "original_Kumari":529,
    "original_Ms ":528,

    "original_surname" :527,
    "original_first_name" :502,
    "original_middle_name" :477,

    "pan_card_abbrevation_part_one" :452,
    "pan_card_abbrevation_part_one" :415,
    "alias_name_yes" : 378,
    "alias_name_no" : 377,


    "alias_Shri" :375,
    "alias_Smt":374,
    "alias_Kumari":373,
    "alias_Ms":372,

    "alias_surname":371,
    "alias_first_name":346,
    "alias_middle_name":321,

    "gender_Male":296,
    "gender_Female":295,
    "gender_Transgender":294,

    "DOB_day":293,
    "DOB_month":291,
    "DOB_year":289,

    "DOP_yes":285,
    "DOP_no":284,

    "DOP_father_surname":283,
    "DOP_father_first_name":258,
    "DOP_father_middle_name":233,

    "DOP_mother_surname":208,
    "DOP_mother_first_name":183,
    "DOP_mother_middle_name":158,

    "selection_father_name":133,
    "selection_mother_name":132,

    "address_line_one":131,
    "address_line_two":106,
    "address_line_three":81,
    "address_line_four":56,
    "address_line_five":31,

    "pincode":6,
}

page_two_rects = {
    
    "office_address_line_one" : 561,
    "office_address_line_two" : 536,
    "office_address_line_three" : 511,
    "office_address_line_four" : 486,
    "office_address_line_five" : 461,
    "office_address_line_six" : 437,
    "office_state" : 0,
    "office_pincode" : 412,
    "office_country_name " : 0,
    
    "address_of_communication_residence": 405,
    "address_of_communication_office":404,
    
    "country_code" : 403,
    "area_code" : 400,
    "telephone_no" : 393,
    
    "status_of_applicant_Government": 379,
    "status_of_applicant_Individual":378,
    "status_of_applicant_Hindu":377,
    "status_of_applicant_Company":376,
    "status_of_applicant_Partnership": 375,
    "status_of_applicant_Association" : 374,
    "status_of_applicant_Trusts" : 373,
    "status_of_applicant_Body" : 372,
    "status_of_applicant_Local" : 371,
    "status_of_applicant_Artificial" : 370,
    "status_of_applicant_Limited" : 369,
    
    "registration_number" : 368,
    "aadhar_number" : 338,
    "enrollment_number" : 326,
    "name_on_aadhar_line_one" : 298,
    "name_on_aadhar_line_two" : 273,
    "name_on_aadhar_line_three" : 248,
    "source_of_income_1" : 222,
    "source_of_income_2" : 221,
    "source_of_income_3" : 220,
    "source_of_income_4" : 217,
    "source_of_income_5" : 216,
    "source_of_income_6" : 215,
    
    "ra_Shri": 213,
    "ra_Smt":212,
    "ra_Kumari":211,
    "ra_Ms ":210,

    "ra_surname" :209,
    "ra_first_name" :184,
    "ra_middle_name" :160,
    
    "ra_address_line_one" : 136,
    "ra_address_line_two" : 111,
    "ra_address_line_three" : 87,
    "ra_address_line_four" : 63,
    "ra_address_line_five" : 38,
    
    "ra_pincode" : 14,
    
}





image = cv2.imread('doc_source/page0.jpg')
image2 = cv2.imread('doc_source/page1.jpg')

#data = {"original_title":"Shri","original_surname":"pasupuleti","original_first_name":"sunil","original_middle_name":"kumar","pan_card_abbrevation":"SUNI KUMAR","alias_":"true","gender":"Male","DOB":"2001-09-17","motherSingleParent_":"false","selection_name":"Fathername","address_line_one":"70-70-70","address_line_two":"Penamaluru","address_line_three":"Thotavari Street","address_line_four":"Penamaluru","address_line_five":"Vijayawada","state":"Andhra Pradesh","pincode":520010,"country_name":"India","office_address":"true","address_of_communication":"residence","country_code":"+91","std_code":"+91","mobile_number":"9959907940","email_id":"sunil.pandvd22@gmail.com","status_of_applicant":"Individual","registration_number":"DOASD8","aadhar_number":"237257737988","enrollment_number":"DJFSDJJO3","name_on_aadhar":"SUNIL KUMAR","ra_title":"Shri","source_of_income":"5.Income from Other sources","ra_surname":"PASUPULETI","ra_first_name":"SUNIL","ra_middle_name":"KUMAR","ra_address_line_one":"70-09-09","ra_address_line_two":"VILLAGE NAME","ra_address_line_three":"Thota vari Street","ra_address_line_four":"CHEPALAKONDI","ra_address_line_five":"Krishna","ra_state":"Andhra Pradesh","ra_pincode":520010,"alias_title":"Shri","alias_surname":"PASUPULETI","alias_first_name":"NANI","alias_middle_name":" KUMAR","DOP_father_surname":"Pasupuleti","DOP_father_first_name":"narasimha ","DOP_father_middle_name":"rao","DOP_mother_surname":"pasupuleti","DOP_mother_first_name":"sri","DOP_mother_middle_name":"devi","office_address_line_one":"Office name","office_address_line_two":"70-70-70","office_address_line_three":"penamaluru","office_address_line_four":"Thotavari Street, High School Road ,","office_address_line_five":"Benz cirlcle","office_address_line_six":"Vijayawada","office_state":"Andhra Pradesh","office_pincode":520010,"office_country_name":"India"}



def annotations(page_one_all_rects,page_two_all_rects):

    print("Hi I am inside annotations")
    #Select title annotator
    annotator(u'\u2713',page_one_all_rects,page_one_rects['original_' + data['original_title']],1,image)
    #Surname title annontator
    annotator(data['original_surname'],page_one_all_rects,page_one_rects['original_surname'],len(data['original_surname']),image)
    #firstName title annontator
    annotator(data['original_first_name'],page_one_all_rects,page_one_rects['original_first_name'],len(data['original_first_name']),image)
    #MiddleName title annontator
    annotator(data['original_middle_name'],page_one_all_rects,page_one_rects['original_middle_name'],len(data['original_middle_name']),image)
    #fullname annontator
    #annotator(data['original_middle_name'],page_one_all_rects,page_one_rects['original_middle_name'],25,image)

    if data['alias_'] == "true":
        #Select alias title annotator
        annotator(u'\u2713',page_one_all_rects,page_one_rects['alias_' + data['alias_title']],1,image)
        #Alias surName title annontator
        annotator(data['alias_surname'],page_one_all_rects,page_one_rects['alias_surname'],len(data['alias_surname']),image)
        #firstName title annontator
        annotator(data['alias_first_name'],page_one_all_rects,page_one_rects['alias_first_name'],len(data['alias_first_name']),image)
        #MiddleName title annontator
        annotator(data['alias_middle_name'],page_one_all_rects,page_one_rects['alias_middle_name'],len(data['alias_middle_name']),image)

    #annotator for gender
    annotator(u'\u2713',page_one_all_rects,page_one_rects['gender_' + data['gender']],1,image)
    #annotator for DoB year
    annotator(data['DOB'][:4],page_one_all_rects,page_one_rects['DOB_year'],4,image)
    #annotator for DoB month
    annotator(data['DOB'][5:7],page_one_all_rects,page_one_rects['DOB_month'],2,image)
    #annotator for DoB month
    annotator(data['DOB'][8:],page_one_all_rects,page_one_rects['DOB_day'],2,image)

    if data['motherSingleParent_'] == 'true':    
        #annotator for gender
        annotator(u'\u2713',page_one_all_rects,page_one_rects['DOP_yes'],1,image)
    else:
        #annotator for gender
        annotator(u'\u2713',page_one_all_rects,page_one_rects['DOP_no'],1,image)

        #father Surname title annontator
        annotator(data['DOP_father_surname'],page_one_all_rects,page_one_rects['DOP_father_surname'],len(data['DOP_father_surname']),image)
        #father firstName title annontator
        annotator(data['DOP_father_first_name'],page_one_all_rects,page_one_rects['DOP_father_first_name'],len(data['DOP_father_first_name']),image)
        #father MiddleName title annontator
        annotator(data['DOP_father_middle_name'],page_one_all_rects,page_one_rects['DOP_father_middle_name'],len(data['DOP_father_middle_name']),image)

    #mother Surname title annontator
    annotator(data['\u200bDOP_mother_surname'],page_one_all_rects,page_one_rects['DOP_mother_surname'],len(data['\u200bDOP_mother_surname']),image)
    #mother firstName title annontator
    annotator(data['DOP_mother_first_name'],page_one_all_rects,page_one_rects['DOP_mother_first_name'],len(data['DOP_mother_first_name']),image)
    #mother MiddleName title annontator
    annotator(data['DOP_mother_middle_name'],page_one_all_rects,page_one_rects['DOP_mother_middle_name'],len(data['DOP_mother_middle_name']),image)

    if data['selection_name'] == 'Mothername':
        #annotator for gender
        annotator(u'\u2713',page_one_all_rects,page_one_rects['selection_mother_name'],1,image)
    else:
        #annotator for gender
        annotator(u'\u2713',page_one_all_rects,page_one_rects['selection_father_name'],1,image)

    annotator(u'\u2713',page_one_all_rects,page_one_rects['original_' + data['original_title']],1,image)
    #address line one annontator
    annotator(data['address_line_one'],page_one_all_rects,page_one_rects['address_line_one'],len(data['address_line_one']),image)
    #address line two annontator
    annotator(data['address_line_two'],page_one_all_rects,page_one_rects['address_line_two'],len(data['address_line_two']),image)
    #address line three annontator
    annotator(data['address_line_three'],page_one_all_rects,page_one_rects['address_line_three'],len(data['address_line_three']),image)
    #address line four annontator
    annotator(data['address_line_four'],page_one_all_rects,page_one_rects['address_line_four'],len(data['address_line_four']),image)
    #address line five annontator
    annotator(data['address_line_five'],page_one_all_rects,page_one_rects['address_line_five'],len(data['address_line_five']),image)

    #address pincode annontator
    annotator(str(data['pincode']),page_one_all_rects,page_one_rects['pincode'],len(str(data['pincode'])),image)

    if data['office_address'] == "true":
        #address line one annontator
        annotator(data['office_address_line_one'],page_two_all_rects,page_two_rects['office_address_line_one'],len(data['office_address_line_one']),image2)
        #address line two annontator
        annotator(data['office_address_line_two'],page_two_all_rects,page_two_rects['office_address_line_two'],len(data['office_address_line_two']),image2)
        #address line three annontator
        annotator(data['office_address_line_three'],page_two_all_rects,page_two_rects['office_address_line_three'],len(data['office_address_line_three']),image2)
        #address line four annontator
        annotator(data['office_address_line_four'],page_two_all_rects,page_two_rects['office_address_line_four'],len(data['office_address_line_four']),image2)
        #address line five annontator
        annotator(data['office_address_line_five'],page_two_all_rects,page_two_rects['office_address_line_five'],len(data['office_address_line_five']),image2)
        #address line six annontator
        annotator(data['office_address_line_six'],page_two_all_rects,page_two_rects['office_address_line_six'],len(data['office_address_line_six']),image2)
        #address pincode annontator
        annotator(str(data['office_pincode']),page_two_all_rects,page_two_rects['office_pincode'],len(str(data['office_pincode'])),image2)

    #address of communication annontator
    annotator(data['address_of_communication'],page_two_all_rects,page_two_rects['address_of_communication_' + data['address_of_communication']],1,image2)


    #address country code annontator
    annotator(str(data['country_code']),page_two_all_rects,page_two_rects['country_code'],len(str(data['country_code'])),image2)
    #address area code annontator
    annotator(str(data['std_code']),page_two_all_rects,page_two_rects['area_code'],len(str(data['std_code'])),image2)
    #address mobile number annontator
    annotator(str(data['mobile_number']),page_two_all_rects,page_two_rects['telephone_no'],len(str(data['mobile_number'])),image2)


    #Status of applicant annotator
    if " " in data['status_of_applicant']:    
        annotator(u'\u2713',page_two_all_rects,page_two_rects['status_of_applicant_' + data['status_of_applicant'].split(" ",1)[1]],1,image2)
    else:
        annotator(u'\u2713',page_two_all_rects,page_two_rects['status_of_applicant_' + data['status_of_applicant']],1,image2)


    #registration number
    annotator(data['registration_number'],page_two_all_rects,page_two_rects['registration_number'],len(str(data['registration_number'])),image2)
    #aadhar number
    annotator(str(data['aadhar_number']),page_two_all_rects,page_two_rects['aadhar_number'],len(str(data['aadhar_number'])),image2)
    #enrollment number
    annotator(str(data['enrollment_number']),page_two_all_rects,page_two_rects['enrollment_number'],len(str(data['enrollment_number'])),image2)

    #Source of income annotator
    annotator(u'\u2713',page_two_all_rects,page_two_rects['source_of_income_' + data['source_of_income'][0]],1,image2)

    #Select title annotator
    annotator(u'\u2713',page_two_all_rects,page_two_rects['ra_' + data['ra_title']],1,image2)
    #Surname title annontator
    annotator(data['ra_surname'],page_two_all_rects,page_two_rects['ra_surname'],len(data['ra_surname']),image2)
    #firstName title annontator
    annotator(data['ra_first_name'],page_two_all_rects,page_two_rects['ra_first_name'],len(data['ra_first_name']),image2)
    #MiddleName title annontator
    annotator(data['ra_middle_name'],page_two_all_rects,page_two_rects['ra_middle_name'],len(data['ra_middle_name']),image2)

    #address line one annontator
    annotator(data['ra_address_line_one'],page_two_all_rects,page_two_rects['ra_address_line_one'],len(data['ra_address_line_one']),image2)
    #address line two annontator
    annotator(data['ra_address_line_two'],page_two_all_rects,page_two_rects['ra_address_line_two'],len(data['ra_address_line_two']),image2)
    #address line three annontator
    annotator(data['ra_address_line_three'],page_two_all_rects,page_two_rects['ra_address_line_three'],len(data['ra_address_line_three']),image2)
    #address line four annontator
    annotator(data['ra_address_line_four'],page_two_all_rects,page_two_rects['ra_address_line_four'],len(data['ra_address_line_four']),image2)
    #address line five annontator
    annotator(data['ra_address_line_five'],page_two_all_rects,page_two_rects['ra_address_line_five'],len(data['ra_address_line_five']),image2)
    #address pincode annontator
    annotator(str(data['ra_pincode']),page_two_all_rects,page_two_rects['ra_pincode'],len(str(data['ra_pincode'])),image2)


    
    cv2.imwrite('image/Image1.jpg',image)
    cv2.imwrite('image/Image2.jpg',image2)

def getRects(file_name):
    #reading the image
    image = cv2.imread(file_name)
    cfg = config.PipelinesConfig()

    # important to adjust these values to match the size of boxes on your image
    cfg.width_range = (30,55)
    cfg.height_range = (25,40)

    # the more scaling factors the more accurate the results but also it takes more time to processing
    # too small scaling factor may cause false positives
    # too big scaling factor will take a lot of processing time
    cfg.scaling_factors = [0.7]

    # w/h ratio range for boxes/rectangles filtering
    cfg.wh_ratio_range = (0.5, 1.7)

    # group_size_range starting from 2 will skip all the groups
    # with a single box detected inside (like checkboxes)
    cfg.group_size_range = (2, 100)

    # num of iterations when running dilation tranformation (to engance the image)
    cfg.dilation_iterations = 0

    from boxdetect.pipelines import get_boxes

    rects, grouping_rects, image, output_image = get_boxes(
        file_name, cfg=cfg, plot=False)
    
    return rects



def renderingEngine(jsondata):
    global data
    print("Hi I am in rendering Engine")
    #global page_one_all_rects
    #global page_two_all_rects
    print('data is here', jsondata)
    data = jsondata
    page_one_all_rects = getRects('doc_source/page0.jpg')
    page_two_all_rects = getRects('doc_source/page1.jpg')

    annotations(page_one_all_rects,page_two_all_rects)

def annotator(field_data,rects,coordinates_loc,max_length,image):
    print("Hi I am here in annotator")
    i=0
    for pos in range(coordinates_loc,coordinates_loc-max_length,-1):
        if i<=len(field_data):            
            x,y,w,h = rects[pos]
            print(field_data,rects[pos])
            font = cv2.FONT_HERSHEY_SIMPLEX
            fontScale = 1
            color = (0,0,0)
            thickness = 2
            org = (x + int(w/3), y + int(5*h/6))
            image = cv2.putText(image, field_data[i].upper(), org, font, fontScale, color, thickness, cv2.LINE_AA)
            i+=1

#renderingEngine(data)


